
var assetHistoryinfo = require('./models/assetHistoryInfo');
var assetCurrentModel = require('./models/assetCurrentInfo');

exports.updateAssetHistory = function (assetID) {
    //update fields of assethistory document with latest data from assetcurrent 
    // and push current history data to update cycles.
    //console.log(assetID);
    
    //update historycycles with current values of assethistoryinfo 

    assetHistoryinfo.findOne({assetId:assetID}, function (err, doc) {
        if (err) console.error(err);

        doc.historyCycles.push({assetId:doc.assetId, cycleEarn:doc.cycleEarn, date: doc.date, capUsed:doc.capUsed, kmDriven: doc.kmDriven,
        actorId: doc.actorId ,cycleNo: doc.cycleNo, cycleState: doc.cycleState});
        doc.save(function (err, updatedTank) {
            if (err) console.error(err);
            //console.log("doc updated");

        });
      }) // update values of assetHistoryinfo with new values from assetcurrentInfo
      .then((doc)=>{
        assetCurrentModel.findOne({assetId: assetID}, function (err, currentDoc) {
            if (err) console.error(err);

            doc.set({ assetId: currentDoc.assetId, capUsed:currentDoc.capUsed , cycleEarn: currentDoc.cycleEarn, kmDriven: currentDoc.kmDriven, cycleNo: (doc.cycleNo+1), actorId: currentDoc.actorId});
            doc.save(function (err, updatedTank) {
                if (err) console.error(err);
              console.log(currentDoc);
            });

      })
    });
}