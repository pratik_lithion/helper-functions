const http = require('http');

exports.upsert = function (assetid, bmsSmsStatus, dataLoggerSmsStatus, lowVolSms, highVolSms,
    tsdSms, highCurrStatus, highCellTemp, highPowerTemp, highAmbientTemp) {

    assetSmsStatus.findOne({ assetId: assetid }, function (err, doc) {

        if (doc) {
            // update the correpsonding assetSmsStatus document 
            doc.set({
                isBmsSmsSent: bmsSmsStatus, isDataLoggerSmsSent: dataLoggerSmsStatus,
                islowVolSmsSent: lowVolSms, ishighVolSmsSent: highVolSms, istsdSmsSent: tsdSms, highCurrStatus: highCurrStatus,
                cellTempStatus: highCellTemp, powerTempStatus: highPowerTemp, ambientTempStatus: highAmbientTemp
            });

            doc.save(function (err, msg) {
                if (err) console.error(err);
            });
        } else {
            //create a new assetSmsStatus document
            var model = new assetSmsStatus({
                assetId: assetid, isBmsSmsSent: bmsSmsStatus, isDataLoggerSmsSent: dataLoggerSmsStatus,
                islowVolSmsSent: lowVolSms, ishighVolSmsSent: highVolSms, istsdSmsSent: tsdSms, highCurrStatus: highCurrStatus,
                cellTempStatus: highCellTemp, powerTempStatus: highPowerTemp, ambientTempStatus: highAmbientTemp
            });

            model.save(function (err, msg) {
                if (err) console.error(err);
            });
        }
    });
}


exports.sendMessage = function (num, msg) {

    num.forEach(function (contact) {

        //console.log(contact.number);
        //console.log(msg);
        let url = `http://alerts.solutionsinfini.com/api/v4/?method=sms&api_key=Ab8978c67bcaa71ca98c5f3eeac1825be&to=${contact.number}&sender=UOLOAP&message=${msg}`;
        console.log(url);
        http.get(url, (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                // zero if msg was sent to only one number
                var res = JSON.parse(data)['data'][0]['status'];
                console.log(res);
                if (res == 'AWAITED-DLR') {
                    console.log("Message Sent");
                    return true;
                }
                else {
                    console.log("There was some error")
                    return false;
                }


            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });

    });
}

//sendMessage(8053095458, "by arrow function");
