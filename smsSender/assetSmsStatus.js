const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const assetSmsSchema = new Schema({

    assetID: String,
    isBmsSmsSent: String,
    islowVolSmsSent: String,
    ishighVolSmsSent: String,
    istsdSmsSent: String,
    highCurrStatus: String,
    isDataLoggerSmsSent: String,
    cellTempStatus: String,
    powerTempStatus: String,
    ambientTempStatus: String
},{
    timestamps:true
});

var assetSmsStatus = mongoose.model('AssetSmsStatus', assetSmsSchema,'assetSmsStatus');
module.exports = assetSmsStatus;