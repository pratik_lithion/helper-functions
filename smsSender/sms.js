const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

mongoose.connect('mongodb://localhost:27017/conFusion');

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function () {
    console.log('connected to the database');
});

var validateBmsData = require('./validateBmsData.js'); //validateBmsData.js
var utility = require('./utility.js');
var AssetSmsStatus = require('./models/assetSmsStatus.js');

// const num = [{ name: "yash", number: "9999499864" }, { name: "Pratik", number: "8860852263" }];
// utility.sendMessage(num, "hello brother");


function smsNotification(extract) {

    var smsStatus = [];
    AssetSmsStatus.findOne({ assetId: extract.assetId }, function (err, doc) {

        if (doc) {
            smsStatus.push(doc.assetID(), doc.isBmsSmsSent(), doc.isDataLoggerSmsSent(), doc.islowVolSmsSent(),
                doc.ishighVolSmsSent(), doc.istsdSmsSent(), doc.highCurrStatus(), doc.cellTempStatus(),
                doc.powerTempStatus(), doc.ambientTempStatus());
        } else {
            smsStatus.push(extract.asset_id, "0", "0", "0", "0", "0", "0", "0", "0", "0");
        }
    });





    var warningResponse = validateBmsData.validateData(extract);

    //var numbers = Constants::$PHONE_NUMBERS;
    const num = [{ name: "yash", number: "9999499864" }, { name: "Randeep", number: "8053095455" }];
    //Low Voltage warning

    if (warningResponse.low_vol_warning == 1 && smsStatus[3] == "0") {
        //warning at least one cell has voltage less than 2.55
        let msg = `Warning!!!At least one cell of assetID ${extract.asset_id} has voltage less than 2.55`;
        utility.sendMessage(num, msg);

        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], "1", smsStatus[4], smsStatus[5], smsStatus[6], smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[3] = "1";
    } else if (warningResponse.low_vol_warning == 0) {
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], "0", smsStatus[4], smsStatus[5], smsStatus[6], smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[3] = "0";
    }

    //Too Low Voltage

    if (warningResponse.low_vol_shut_down == 1 && smsStatus[5] == "0") {
        //at least one cell voltage is less than 2.51 shutting down bms now
        let msg = `At least one cell of assetID  ${extract.asset_id} has voltage less than 2.51`;
        utility.sendMessage(num, msg);
        //Don't have numbers stored for every asset id so can't shut down that asset now
        //$smsSender->sendMessage("", 'OA');

        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], "1", smsStatus[6], smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[5] = "1";
    } else if (warningResponse.low_vol_shut_down == 0) {
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], "0", smsStatus[6], smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[5] = "0";
    }

    //High Voltage Warning


    if (warningResponse.high_vol_warning == 1 && smsStatus[4] == "0") {
        //warning at least one cell voltage reached to 3.65
        let msg = `Warning!!! At least one cell of assetID+ ${extract.asset_id}  has voltage higher than 3.65`;
        utility.sendMessage(num, msg);

        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], "1", smsStatus[5], smsStatus[6], smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[4] = "1";
    } else if (warningResponse.high_vol_warning == 0) {
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], "0", smsStatus[5], smsStatus[6], smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[4] = "0";
    }

    //High Current Warning

    if (warningResponse.high_current == 1 && smsStatus[6] == "0") {
        //here we will handle if current is more than 40 at the time of discharging
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], "1", smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[6] = "1";
    } else if (warningResponse.high_current == 1 && smsStatus[6] == "1") {
        //            $msg = "Warning!!!+High+Current+Usage+by+assetid+".$extract.asset_id;
        //            $smsSender->sendMessage($numbers, $msg);
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], "2", smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[6] = "2";
    } else if (warningResponse.high_current == 1 && smsStatus[6] == "2") {
        msg = `Alert!!! Asset ID ${extract.asset_id} is using high current so long`;
        utility.sendMessage(num, msg);

        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], "3", smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[6] = "3";
        //$smsSender->sendMessage("", 'OA');
    } else if (warningResponse.high_current == 1 && smsStatus[6] == "3") {
        msg = `Alert!!! Asset ID ${extract.asset_id} is still using high current`;
        utility.sendMessage(num, msg);
    } else if (warningResponse.high_current == 0 && smsStatus[6] !== "0") {
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], "0", smsStatus[7], smsStatus[8], smsStatus[9]);
        smsStatus[6] = "0";
    }


    //High Cell Temperature warning


    //        if (1 === $r['high_cell_temp_warning'] && ("0" === $smsStatus[7] || ($parsedArray[0]=="1" && $parsedArray[1] >= "02.00"))) {
    //            //warning cell temp more than 43
    //            $msg = "Warning!!!+At+least+one+Cell+Temperature+of+assetID+".$extract.asset_id."+is+more+than+43.";
    //            $smsSender->sendMessage($numbers, $msg);
    //            if($parsedArray[0]=="1" && $parsedArray[1] >= "02.00"){
    //                $stopNotAllowedCharging->stopHighTempCharging($context, $extract.asset_id);
    //            }
    //           $this->upsertStatus($context, $extract.asset_id, $smsStatus[1], $smsStatus[2], $smsStatus[3], $smsStatus[4], $smsStatus[5], $smsStatus[6], "1", $smsStatus[8], $smsStatus[9]);
    //            $smsStatus[7] = "1";
    //        } elseif (($stopNotAllowedCharging->stopNotAllowedCharging($context, $extract.asset_id, $parsedArray[0], $parsedArray[1], $parsedArray[22], $parsedArray[23]) == 0)
    //            && (0 === $r['high_cell_temp_warning']) ) {
    //
    //            $msg = "Starting+Charging+for+".$extract.asset_id."+now+temperature+is+less+than+40.";
    //            $smsSender->sendMessage($numbers, $msg);
    //
    //            $startCharging->startCharging($context, $extract.asset_id);
    //            $this->upsertStatus($context, $extract.asset_id, $smsStatus[1], $smsStatus[2], $smsStatus[3], $smsStatus[4], $smsStatus[5], $smsStatus[6], "0", $smsStatus[8], $smsStatus[9]);
    //            $smsStatus[7] = "0";
    //        } elseif (0 === $r['high_cell_temp_warning'] || 2 === $r['high_cell_temp_warning']) {
    //            $stopNotAllowedCharging->stopNotAllowedCharging($context, $extract.asset_id, $parsedArray[0], $parsedArray[1], $parsedArray[22], $parsedArray[23]) ;
    //            $this->upsertStatus($context, $extract.asset_id, $smsStatus[1], $smsStatus[2], $smsStatus[3], $smsStatus[4], $smsStatus[5], $smsStatus[6], "0", $smsStatus[8], $smsStatus[9]);
    //            $smsStatus[7] = "0";
    //        }

    //High Power Temperature warning


    if (warningResponse.high_power_temp_warning == 1 && smsStatus[8] == "0") {
        //warning power temp more than 80
        msg = `Warning!!! Power Temperature of assetID ${extract.asset_id} is more than 80.`;
        utility.sendMessage(num, msg);

        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], smsStatus[6], smsStatus[7], "1", smsStatus[9]);
        smsStatus[8] = "1";
    } else if (warningResponse.high_power_temp_warning == 0) {
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], smsStatus[6], smsStatus[7], "0", smsStatus[9]);
        smsStatus[8] = "0";
    }

    //High Ambient Temperature warning

    if (warningResponse.high_ambient_temp_warning == 1 && smsStatus[9] == "0") {
        //warning ambient temp more than 60
        //            $msg = "Warning!!!+Ambient+Temperature+of+assetID+".$extract.asset_id."+is+more+than+60";
        //            $smsSender->sendMessage($numbers, $msg);

        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], smsStatus[6], smsStatus[7], smsStatus[8], "1");
        smsStatus[9] = "1";
    } else if (warningResponse.high_ambient_temp_warning == 0) {
        utility.upsert(context, extract.asset_id, smsStatus[1], smsStatus[2], smsStatus[3], smsStatus[4], smsStatus[5], smsStatus[6], smsStatus[7], smsStatus[8], "0");
        smsStatus[9] = "0";
    }
}

