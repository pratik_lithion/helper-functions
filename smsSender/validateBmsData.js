exports.validateData = function(data)
{
    var curr = extract.current;
    var cellTemp = [data.extTemp1, data.extTemp2, data.extTemp3, data.extTemp4, data.extTemp5];
    var lowVolOff = [];
    var lowVolWarn = [];
    var highVolWarn = [];
    var cellVoltage= [data.v1,data.v2,data.v3,data.v4,data.v5,data.v6,data.v7,data.v8,data.v9,data.v10,data.v11,data.v12,data.v13,data.v14,
                     data.v15,data.v16];

    cellVoltage.forEach(function(cellVol) {

        if (cellVol <= "2.55" && cellVol!=="0.00") {
            if (cellVol <= "2.51") {
                //array for BMS OFF message
                lowVolOff.push(i);
            } else {
                //array for warning message
                lowVolWarn.push(i);
            }
        } 
        else if (cellVol >= "3.65") {
            //array for High Voltage Warn Message
            highVolWarn.push(i);
        }
    });
    
    var highCellTemp = 0;

    //High temperature of any cell
    for (i=0 ; i<5 ; i++){
        if(cellTemp[i] >= "43"){
            highCellTemp = 1;
            break;
        }
    }

    // Normal temperature of all
    if(cellTemp[0] <="40" && cellTemp[1]<="40" && cellTemp[2]<="40" && cellTemp[3]<="40" && cellTemp[4]<="40"){
        highCellTemp = 0;
    }

    // if(cellTemp[0] >"40" && cellTemp[1]>"40" && cellTemp[2]>"40" && cellTemp[3]>"40" && cellTemp[4]>"40" && highCellTemp != 1){
    //     highCellTemp = 2;
    // }

    var res = {
        low_vol_warning:0,
        low_vol_shut_down:0,
        high_vol_warning:0,
        high_current:0,
        high_cell_temp_warning:0,
        high_power_temp_warning:0,
        high_ambient_temp_warning:0
    };

    if (lowVolWarn.length>0)  res.low_vol_warning = 1 ; 
    if (lowVolOff.length>0)  res.low_vol_shut_down = 1 ;
    if (highVolWarn.length>0)  res.high_vol_warning = 1 ;
    if (curr >= 40.00) res.high_current = 1;
    if (highCellTemp != 0) res.high_cell_temp_warning = 1;
    if (extract.powerTemp >= 80) res.high_power_temp_warning = 1;
    if (extract.ambientTemp >= 60) res.high_ambient_temp_warning = 1; 


    return res;
}

